from discord import Embed, Colour, User, Member
from discord.ext import commands
from random import randint
from typing import Optional

MIN_STEAL_AMOUNT=30
MAX_STEAL_AMOUNT=2000
LEADERBOARD_EMOJI_KEY={1:"\N{CROWN}", 2:"\N{TRIDENT EMBLEM}", 3: "\N{TROPHY}"}
LEADERBOARD_DEFAULT_EMOJI="\N{REMINDER RIBBON}"


class Currency(commands.Cog):
    # TODO: Make it so you can steal from other users,
    # but all you can do with that money is use it to
    # steal from more people: it's literally useless.
    # You can steal an infinite amount from the bot
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.db = bot.db

    @commands.command(aliases=["bal"])
    @commands.cooldown(3, 10, commands.BucketType.user)
    async def balance(self, ctx, member: Member="self"):
        """Find a user's balance"""
        member = ctx.author if member == "self" else member
        moolah = await self.db.get_user_money(member.id)
        await ctx.send(f"**{member}** has `${moolah}`")

    @commands.command(aliases=["lb", "topusers"])
    async def leaderboard(self, ctx):
        """Shows who has the most amount
        of money in this server"""
        cache = self.db.cache
        users = cache.get("users")
        ids = [mbr.id for mbr in ctx.guild.members if not mbr.bot]
        b = {id_:users[id_] for id_ in users if users[id_].get("money")!=None} if users else dict()
        lb = {int(id_): data.get("money", 0) for id_, data in b.items() if int(id_)}
        embed = Embed(colour=Colour.blue(), title="The richest users in this server", description="")
        embed.set_footer(text=f"Richest users in {ctx.guild}", icon_url=ctx.guild.icon_url)

        for id_ in sorted(lb.keys(), key=lambda u:-lb[u])[:10]:
            money = lb[id_]
            emoji = LEADERBOARD_EMOJI_KEY.get(len(embed.description.split("\n")), LEADERBOARD_DEFAULT_EMOJI)
            embed.description += f"\n{emoji} **{self.bot.get_user(id_)}** has `${money:,}`"
        await ctx.send(embed=embed)

    @commands.command(aliases=["freemoney"])
    @commands.cooldown(1, 60*60*4, commands.BucketType.user)
    async def daily(self, ctx):
        """Gives you a random amount of money between
        300 and 400 dollars into your wallet"""
        amount = 300+randint(0, 75)
        await ctx.send(f"Redeemed your daily cheque for `${amount}` :thumbsup:")
        await self.db.add_user_money(ctx.author.id, amount)
    
    @commands.command(enabled=False)
    @commands.guild_only()
    @commands.cooldown(1, 30, commands.BucketType.member)
    async def steal(self, ctx, victim: Member):
        """Attempts to steal from <victim>\n
        Both you and the user you are attemping to
        steal from must have at least $75 or this
        will fail

        There is a 35% chance that you will fail
        and, as compensation, give the user you
        attempted to steal from some money

        The only bot that you can steal from is this one.
        However, there is a 60% chance that you will fail,
        but also a 125% reward if you are successful"""
        you = await self.db.get_user_money(ctx.author.id, human_readable=False)
        vtm = await self.db.get_user_money(victim.id, human_readable=False)
        if victim.bot and victim != ctx.guild.me:
            return await ctx.send(f"Uh oh, you can't steal from bots! \N{ROBOT FACE} ~~||except me||~~")
        if you < 75:
            return await ctx.send(f"You must have at least `$75` to steal from someone. You still need another `${75-you}` \N{SHRUG}")
        if vtm < 75 and victim != ctx.guild.me:
            return await ctx.send(f"Not worth it, **{victim}** only has `${vtm}`")

        win = randint(0, 100)>90#35
        amount = randint(MIN_STEAL_AMOUNT, MAX_STEAL_AMOUNT)

        if win:
            while amount > vtm:
                amount = randint(MIN_STEAL_AMOUNT, amount)
            
            await ctx.send(f"Wow congrats **{ctx.author}**! You managed to steal `${amount}` from **{victim}**")
            try: await victim.send(f"Massive 🇫 {ctx.author.mention} just stole `${amount}` from you in `{ctx.guild}` \N{CONFUSED FACE}")
            except: pass
        else:
            while amount > you:
                amount = randint(MIN_STEAL_AMOUNT, amount)
            
            msg=await ctx.send(f"Massive 🇫 for **{ctx.author}** who tried (and failed) steal from **{victim}** and had to pay them `${amount}`")
            try: await msg.add_reaction("🇫")
            except: pass
            amount=-amount

        # Transfer the money
        #await self.db.give_user_money(ctx.author.id, amount)
        #await self.db.give_user_money(victim.id, -amount)

    @commands.command()
    async def flip(self, ctx, headsortails:str=""):
        """Flip a coin, who knows, you might get some money"""
        call = headsortails.lower()
        esp = "heads" if randint(0, 1) else "tails"
        
        if call not in ["heads", "tails"]:
            return await ctx.send("You need to call either `heads` or `tails`. It's that obvious")
        if call != esp:
            await ctx.send(f"massive 🇫 It was `{esp}` and you picked `{call}` \N{MONEY BAG} Waste of a coin lmao")
            return await self.db.add_user_money(ctx.author.id, -1)
        await ctx.send(f"Congrats you have ESP. As a result, you were rewarded with `$1`")
        await self.db.add_user_money(ctx.author.id, 1)

class AdminCurrency(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.db = bot.db
    
    @commands.command()
    @commands.is_owner()
    async def givemoney(self, ctx, user: Optional[Member]=None, amount=100):
        usr = user or ctx.author
        await self.db.add_user_money(usr.id, amount)
        await ctx.send(f":thumbsup: Gave `${amount}` to **{usr}**")

def setup(bot):
    bot.add_cog(Currency(bot))
    bot.add_cog(AdminCurrency(bot))
