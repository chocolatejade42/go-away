"""Lmao literally every command in this cog is in jsk"""
import ast
from asyncio import TimeoutError as AsyncioTimeoutError
from datetime import datetime as dt
from inspect import getsource, getsourcefile, getsourcelines
from os.path import relpath
from time import time

import discord
from discord.ext import commands


"""
Don't star import from modules `commands` and `discord`
because they are directly referenced
"""


class Developer(commands.Cog):
    """Developer commands to manage the bot"""

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.command(aliases=["rld"])
    @commands.is_owner()
    async def reload(self, ctx, *cogs):
        """Reload cogs on the bot\n
        If param `cogs` is empty, all cogs will be reloaded 👍"""
        await ctx.trigger_typing()
        e = discord.Embed(colour=discord.Colour.blue(), timestamp=dt.utcnow())
        e.set_footer(
            text="Cogs in bold have already been reloaded",
            icon_url=self.bot.user.avatar_url,
        )

        if cogs != tuple():
            # Cleanup
            cogs = tuple([f"cogs.{cog}" for cog in cogs])
        else:
            # Reload whole bot
            cogs = tuple(self.bot.extensions)

        e = discord.Embed(
            colour=discord.Colour.blue(),
            description="Reloading cogs `" + "`, `".join(cogs) + "`",
        )
        msg, start_time = await ctx.send(embed=e), time()
        scogs, fcogs = [], []  # list and list of tuple (cog, error)
        for cog in cogs:
            try:
                self.bot.reload_extension(cog)
            except Exception as err:
                fcogs.append((cog, err))
            else:
                scogs.append(cog)

        if scogs:
            e.add_field(name="Successful cogs ✅", value="`" + "`, `".join(scogs) + "`")
        if fcogs:
            e.add_field(
                name="Failed cogs ❌",
                value="\n".join([f"`{i[0]}` **-** {str(i[1])}" for i in fcogs]),
                inline=False,
            )

        e.set_author(name="Cog reload complete", icon_url=self.bot.user.avatar_url)
        e.description = f"Finished reloading `{len(cogs)}` cogs in `{round((time()-start_time)*1000)}`ms ⚙"
        await msg.edit(embed=e)

    @commands.command(enabled=False)
    async def source(self, ctx, *, command: str=None):
        """Displays my full source code or for a specific command.
        To display the source code of a subcommand you can separate it by
        periods, e.g. tag.create for the create subcommand of the tag command
        or by spaces.
        """
        source_url = "https://github.com/aiden2480/moopity-moop"
        if command is None:
            return await ctx.send(source_url)

        if command == "help":
            src = type(self.bot.help_command)
            module = src.__module__
            filename = getsourcefile(src)
        else:
            obj = self.bot.get_command(command.replace(".", " "))
            if obj is None:
                return await ctx.send("Could not find command.")

            src = obj.callback.__code__
            module = obj.callback.__module__
            filename = src.co_filename

        lines, firstlineno = getsourcelines(src)
        location = relpath(filename).replace("\\", "/")

        final_url = f"<{source_url}/blob/master/{location}#L{firstlineno}-L{firstlineno + len(lines) - 1}>"
        await ctx.send(final_url)

    @commands.command()
    @commands.is_owner()
    async def shutdown(self, ctx):
        """Shutdown the bot, bye! 👋"""
        await ctx.send("Bye! 👋")
        await self.bot.logout()
        raise KeyboardInterrupt(f"Shutdown command run by {ctx.author}")

    @commands.is_owner()
    @commands.command(hidden=True)
    async def evalu(self, ctx, user: discord.Member, *, command: str):
        """Evaluate commands as another user
        (testing owner-only commands and the like)"""
        ctx.message.author = user
        ctx.message.content = f"{ctx.prefix}{command}"
        await self.bot.process_commands(ctx.message)


def setup(bot: commands.Bot):
    bot.add_cog(Developer(bot))
