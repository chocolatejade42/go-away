from datetime import datetime as dt

from discord import AsyncWebhookAdapter, Embed, Guild, Member, Webhook, Role
from discord.ext import commands


class Events(commands.Cog):
    """Handles all extra events for the bot that
    I don't want clogging up the main file"""
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.db = bot.db
        self.sess = bot.session
        self.guild_webhook_url = bot.env["GUILD_WEBHOOK_URL"]
        self.commands_webhook_url = bot.env["COMMANDS_WEBHOOK_URL"]

    # Webhook events
    @commands.Cog.listener(name="on_guild_join")
    async def guild_join_webhook(self, guild: Guild):
        """Notify me when the bot joins a guild"""
        self.bot.guildlogger.info(f"Joined a guild: {guild.name}")
        webhook = Webhook.from_url(self.guild_webhook_url, adapter=AsyncWebhookAdapter(self.sess))
        e = Embed(
            colour=0x6CE479,
            timestamp=dt.utcnow(),
            description=f"Joined **{guild.name}**",
        )

        e.set_thumbnail(url=guild.icon_url)
        e.add_field(name="Member count", value=guild.member_count)
        e.add_field(name="New total guilds", value=len(self.bot.guilds))
        e.set_author(name="Guild Join", icon_url="https://bit.ly/32iG9BC")
        e.set_footer(text=f"Guild owner: {guild.owner}", icon_url=guild.owner.avatar_url)

        await webhook.send(
            embed=e,
            username=self.bot.user.name,
            avatar_url=self.bot.user.avatar_url,
        )

    @commands.Cog.listener(name="on_guild_remove")
    async def guild_leave_webhook(self, guild: Guild):
        """Notify me when the bot leaves a guild"""
        self.bot.guildlogger.info(f"Left a guild: {guild.name}")
        webhook = Webhook.from_url(self.guild_webhook_url, adapter=AsyncWebhookAdapter(self.sess))
        e = Embed(
            colour=0xE84C3D,
            timestamp=dt.utcnow(),
            description=f"Left **{guild.name}**",
        )

        e.set_thumbnail(url=guild.icon_url)
        e.add_field(name="Member count", value=guild.member_count)
        e.add_field(name="New total guilds", value=len(self.bot.guilds))
        e.set_author(name="Guild Leave", icon_url="https://bit.ly/2NQSABA")
        e.set_footer(text=f"Guild owner: {guild.owner}", icon_url=guild.owner.avatar_url)

        await webhook.send(
            embed=e,
            username=self.bot.user.name,
            avatar_url=self.bot.user.avatar_url,
        )

    @commands.Cog.listener(name="on_command")
    async def command_run_webhook(self, ctx: commands.Context):
        # TODO: Needs to be reformatted?
        self.bot.cmdlogger.info(f"{ctx.author}: {ctx.message.content}")
        webhook = Webhook.from_url(self.commands_webhook_url, adapter=AsyncWebhookAdapter(self.sess))
        e = Embed(
            colour=0xFFC000,
            timestamp=dt.utcnow(),
            description=f"```{ctx.message.clean_content}```",
        )

        if ctx.guild:
            e.set_thumbnail(url=ctx.guild.icon_url)
            e.add_field(name="Guild", value=ctx.guild.name)

        e.add_field(name="Channel", value=f"{'#' if ctx.guild else ''}{ctx.channel}")
        e.set_author(name="Command run", icon_url="https://bit.ly/2p75wYc")
        e.set_footer(text=f"{ctx.author} • {ctx.author.id}", icon_url=ctx.author.avatar_url)

        await webhook.send(
            embed=e,
            username=self.bot.user.name,
            avatar_url=self.bot.user.avatar_url,
        )

    # Minecraft role events
    @commands.Cog.listener(name="on_member_update")
    async def minecraft_role(self, before: Member, after: Member):
        """Gives the `Minecraft` role to a user in my server if
        they start playing it\n
        Takes the role away if they stop playing it
        """
        guild = before.guild
        if not guild:
            return
        if str(guild.id) not in self.db.guild_minecraft_roles.keys():
            return
        
        role = guild.get_role(int(self.db.guild_minecraft_roles[str(guild.id)]))
        b = [a.name for a in before.activities]
        a = [a.name for a in after.activities]

        if "Minecraft" in a and "Minecraft" not in b:
            try:
                await after.add_roles(role, reason="Started playing Minecraft")
                self.bot.logger.debug(f"Adding minecraft role to {before}")
            except: pass # Missing permissions
        if "Minecraft" in b and "Minecraft" not in a:
            try:
                await after.remove_roles(role, reason="Stopped playing Minecraft")
                self.bot.logger.debug(f"Removing minecraft role from {before}")
            except: pass # Missing permissions

    @commands.Cog.listener(name="on_guild_role_delete")
    async def check_minecraft_role_deleted(self, role: Role):
        # TODO: Maybe make a logger for `events`? (`guild` can fall under it)
        self.bot.logger.debug(f"DELETED {role.name!r} {role.id} {role.guild}")
        if str(role.id) in self.db.guild_minecraft_roles.values():
            self.bot.logger.debug("This role was a minecraft role")
            await self.db.set_minecraft_role(role.guild.id, None)
            self.bot.logger.debug(f"Deleted the minecraft role for {role.guild.id} ({role.id})")
    
    # Delete guild data >:)
    @commands.Cog.listener(name="on_guild_join")
    async def thanos_snap(self, guild: Guild):
        if not self.bot.delete_guild_data: return
        # FIXME: Pickup from here
        print(f"deleting guild data for {guild.id}")
        await self.db.delete_guild(guild.id)

    # Blacklist event
    @commands.Cog.listener(name="on_guild_join")
    async def check_blacklist_guilds(self, guild: Guild):
        if not await self.db.is_guild_blacklisted(guild.id):
            return
        
        for channel in guild.text_channels:
            perms = channel.permissions_for(guild.me)
            if perms.send_messages:
                chnl = channel
                break
        
        aidzman = await self.bot.fetch_user(self.bot.owner_id)
        await chnl.send(f"Hello residents of **{guild.name}** \N{WAVING HAND SIGN} It looks like you've been blacklisted from using this bot \N{CONFUSED FACE}\nIf you think this is a mistake, please contact `{aidzman}` to undo this ban\nI'll just leave now \N{DOOR}")
        await guild.leave()



def setup(bot: commands.Bot):
    bot.add_cog(Events(bot))
