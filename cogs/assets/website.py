from logging import getLogger
from os import getenv
from time import time
from urllib.parse import urlencode

from aiohttp import ClientSession, web
from aiohttp_jinja2 import template
from aiohttp_session import get_session, new_session

# Create objects
OAUTH_SCOPE = "identify guilds"
routes = web.RouteTableDef()
logger = getLogger("web")

# Routes
@routes.get("/")
@template("index.jinja")
async def index(request: web.Request):   
    sess = await get_session(request)

    try: i = sess["user_info"]
    except KeyError: i = "lol no"
    print(i)

    start = time()
    data = await request.app["db"].client.get("")

    return dict(res=data, time=time, start=start)


@routes.get("/info")
@template("info.jinja")
async def info(request: web.Request):
    return dict()


@routes.get("/stats")
@template("stats.jinja")
async def stats(request: web.Request):
    return dict()


@routes.get("/cmds")
@template("cmds.jinja")
async def cmds(request: web.Request):
    return dict(data=request.app.cmddata)


@routes.get("/links")
@template("links.jinja")
async def links(request: web.Request):
    return dict()


# Star routes
@routes.get("/invite")
@template("invite.jinja")
async def invite(request: web.Request):
    """Handles all the bot invite routes"""
    p = request.query.get("p")

    if p == "select": url=request.app["bot"].invite_url(-1)
    elif p == "none": url=request.app["bot"].invite_url(0)
    else: url=request.app["bot"].invite_url()
    return web.HTTPFound(url)


@routes.get("/login")
async def login(request: web.Request):
    # Get the code
    code = request.query.get("code")
    oauth = get_oauth(request)
    if not code:
        return web.HTTPFound(oauth["url"])

    # Generate the post data
    data = {"grant_type": "authorization_code", "code": code, "scope": OAUTH_SCOPE}
    data.update(oauth["data"])
    headers = {"Content-Type": "application/x-www-form-urlencoded"}

    # Make the request
    async with ClientSession(loop=request.loop) as session:

        # Get auth
        token_url = "https://discordapp.com/api/v6/oauth2/token"
        async with session.post(token_url, data=data, headers=headers) as r:
            token_info = await r.json()

        # Get user
        headers.update({"Authorization": f"{token_info['token_type']} {token_info['access_token']}"})
        user_url = "https://discordapp.com/api/v6/users/@me"
        async with session.get(user_url, headers=headers) as r:
            user_info = await r.json()

        # Get guilds
        guilds_url = f"https://discordapp.com/api/v6/users/@me/guilds"
        async with session.get(guilds_url, headers=headers) as r:
            guild_info = await r.json()

    # Save to session
    # TODO: This doesn't save reeee
    sess = await new_session(request)

    # Update data
    user_info["avatar_url"] = get_avatar(user_info)
    sess["user_info"] = user_info
    sess["guild_info"] = guild_info
    print(sess["user_info"]["avatar_url"])

    # Redirect to home
    sess["logged_in"] = int(user_info["id"])
    print(sess["logged_in"])
    return web.HTTPFound(location="/")


@routes.get("/logout")
async def logout(request: web.Request):
    sess = await get_session(request)
    sess.invalidate()
    return web.HTTPFound("/")


# Other functions
async def web_get_cmd_data(app: web.Application):
    """Gets the command data from the bot\n
    This should be added as an `on_startup` signal"""
    cmds = app["bot"].commands
    cogs = {cmd.cog.__class__.__name__ for cmd in cmds if cmd.cog}
    data = {cog: list() for cog in cogs}

    for cmd in cmds:
        c = cmd.cog.__class__.__name__ if cmd.cog else "General"
        chks = [chk.__qualname__.split(".")[0] for chk in cmd.checks]
        #print(cmd.name, chks)
        check = lambda cmd: all(
            (not cmd.hidden, cmd.enabled, "is_owner" not in chks, cmd.name != "jishaku")
        )
        if check(cmd):
            data[c].append(cmd)
    for cog in data.copy():
        if data[cog] == list():
            del data[cog]

    app.cmddata = data

def get_oauth(request: web.Request):
    # TODO: Make a better way to create oauth objects
    app = request.app
    app.logger.debug("Creating oauth objects")    

    oauth_data = {
        "client_id": app["bot"].user.id,
        "client_secret": getenv("BOT_CLIENT_SECRET"),
        "redirect_uri": app["bot"].oauth_callback
    }
    oauth_url = "https://discordapp.com/api/oauth2/authorize?" + urlencode({
        "client_id": app["bot"].user.id,
        "redirect_uri": app["bot"].oauth_callback,
        "response_type": "code",
        "scope": OAUTH_SCOPE,
    })
    return dict(data=oauth_data, url=oauth_url)

def get_avatar(user_info:dict={}):
    try: return f"https://cdn.discordapp.com/avatars/{user_info['id']}/{user_info['avatar']}.png"
    except KeyError:
        try: return f"https://cdn.discordapp.com/embed/avatars/{int(user_info['discriminator']) % 5}.png"
        except KeyError: pass
    return "https://cdn.discordapp.com/embed/avatars/0.png"
