# Moopity Moop
The repo for my bots code 😀 See https://moopity-moop.chocolatejade42.repl.co/ for the bots website (I know, I need to get a better domain 😒)

## Todo list
Stuff I have to do is unmarked, and stuff I'm slowly working on is marked. Any comment with `TODO` in it anywhere throughout the repo is also something I have to do but I can't be bothered to put it on here lmao
- [ ] Fix up whatever useless shit was going through my brain the last time I worked on this because the code looks like crap. 
- [x] Actually hook up GitHub with repl.it so when I push updates it'll actually update the bot lmao
- [x] Change the website from `flask` to `aiohttp`
    - LOG IN
        - Use cookies that expire in a month or something to retain logged in (The cookie can just be an encrypted version of the users API key)
        - Once logged in, provide access to the path `/guildsettings` which is just a lit of all the users guilds which redirects to `/guild/{guild.id}`
    
- [ ] Make sure `@commands.command` is the first decorator on any command
- [ ] Fix whatever fuckery black did with my beautiful syntax
- [ ] Create better command names/aliases
- [ ] Test for the `Forbidden` error that discord throws when Moopity Moop can't speak but wants to send a message
- [ ] Rip off the help command from https://github.com/nguuuquaaa/Belphegor/blob/master/belphegor/help.py because that was a cool help command
- [ ] Rip off the source command from https://github.com/nguuuquaaa/Belphegor/tree/master/belphegor/help.py#L479-L512 because that was a cool source command
- [x] Make sure all ClientSessions are referenced as `sess` not `http` because that fucks with discords internal cache
- [ ] The website is literally leagues behind the bot and the bot is complete shit so I really need to get `aiohttp-session` to get its shit together and just *work*
